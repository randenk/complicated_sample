<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models;
use App\Models\NoSQL\NoSqlEntity;
use DB;
use Illuminate\Support\Carbon;
use App\Services\TenancyService;
use App\Traits\MetricsTrait;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Exception;
use Illuminate\Support\Facades\Schema;
use MongoDB\BSON\ObjectId;

class AccountPrune extends Command
{
    use MetricsTrait;

    protected $opt;
    protected $tempTableUid;
    protected $tableNames;
    protected $tempTables;
    protected $counts;
    protected $deleteQueries;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pvbid:account:prune 
        {--account-id= : The ID of the account to prune} 
        {--days-prior= :  The amount of days prior (integer)}
        {--skip-confirmation : Confirm that these arguments are correct}
        {--dry-run : Whether or not to actually perform the deletes}
        {--run-optimize : Should we optimize the tables at the end?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Permanently delete soft deleted records relating to the 
        given account back to a specified amount of days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->deleteQueries = (object) [];
        $this->counts = (object) [];
        $this->tempTableUid = new ObjectId();
        $this->tableNames = (object) [
            'project' => (new Models\Project)->getTable(),
            'bid' => (new Models\Bid)->getTable()
        ];
        $this->entityConfigs = config('entities.list')->filter(function ($item) {
            return $item['type'] !== 'bid';
        });
    }

    /**
     * Sets all the options
     *
     * @return void
     */
    protected function handleOptions()
    {
        $days = $this->option('days-prior') ?? $this->ask('How many days back do you want to delete prior?');

        $this->opt = (object) [
            'isDryRun' => $this->option('dry-run'),
            'shouldConfirm' => !$this->option('skip-confirmation'),
            'shouldOptimize' => $this->option('run-optimize'),
            'acctId' => $this->option('account-id') ?? $this->ask('What is the account id?'),
            'daysPriorTs' => Carbon::now()->subDays($days)
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->handleOptions();

        $this->opt->isDryRun
            ? $this->info("... DRY RUN ...")
            : $this->startMetricsTimer();

        TenancyService::switchDB($this->opt->acctId);
        $acct = Models\Account::findOrFail($this->opt->acctId);
        $acctLabel = "({$this->opt->acctId}) {$acct->title}";

        if (
            !$this->opt->isDryRun
            && $this->opt->shouldConfirm
            && !$this->confirm("You are about to prune account $acctLabel. Are you sure you want to proceed?")
        ) {
            $this->info('exiting...');
            die;
        }

        $this->info("... Running on account $acctLabel ...");

        try {
            if ($this->getOutput()->isDebug()) {
                DB::connection()->enableQueryLog();
            }

            DB::beginTransaction();

            $tempTables = (object) [];
            $tempTables->projects = $this->gatherProjects();
            $tempTables->bids = $this->gatherBids($tempTables->projects);
            $this->handleEntities($tempTables->bids);

            if (!empty($this->counts->bid)) {
                $this->handleNoSqlBids($this->getIds($this->deleteQueries->bid));
            }

            if (!$this->opt->isDryRun) {
                foreach ($this->entityConfigs as $entityConfig) {
                    $this->performDelete($entityConfig);
                }

                $this->performDelete(['type' => 'noSqlBid', 'entityClass' => Models\NoSQL\Bid::class]);
                $this->performDelete(['type' => 'bid', 'entityClass' => Models\Bid::class]);
                $this->performDelete(['type' => 'project', 'entityClass' => Models\Project::class]);


                DB::commit();
            }
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }

        if ($this->getOutput()->isDebug()) {
            $this->info('Query Log:' . json_encode(DB::getQueryLog()), 'vvv');
        }

        if (!$this->opt->isDryRun) {
            $this->info('... Prune complete! ...');
            $this->endMetricsTimer();
        }
    }

    /**
     * Override for the info output - prefixes a timestamp
     *
     * @param string $msg
     * @param int|null $verbosity
     * @return void
     */
    public function info($msg, $verbosity = null)
    {
        $ts = Carbon::now();
        parent::info("$ts - $msg", $verbosity);
    }

    /**
     * Gather the Projects IDs into a temp table
     * With a flag to denote which should be deleted
     *
     * @return string The temp table name
     */
    protected function gatherProjects(): string
    {
        $this->info('Collecting Project IDs...', 'v');

        $tempTable = "acct_prune_projects_temp_{$this->tempTableUid}";
        DB::statement("
            CREATE TEMPORARY TABLE {$tempTable}
            (id INTEGER NOT NULL, PRIMARY KEY(id), INDEX compound_key (id, should_delete))
            SELECT 
                id,
                IF (deleted_at <= :daysPriorTs, true, false) as should_delete
            FROM {$this->tableNames->project}
            WHERE account_id = :accountId
        ", ['accountId' => $this->opt->acctId, 'daysPriorTs' => $this->opt->daysPriorTs]);

        $this->deleteQueries->project = DB::table("{$this->tableNames->project} AS source")
            ->join("$tempTable AS pTemp", 'pTemp.id', '=', 'source.id')
            ->where('source.account_id', $this->opt->acctId)
            ->where('pTemp.should_delete', '=', true);

        $this->count('project', DB::table("$tempTable AS source")->where('should_delete', '=', true));

        return $tempTable;
    }

    /**
     * Gather the Bid IDs into a temp table
     * With a flag to denote which should be deleted
     *
     * @param string $projectIdsTempTbl
     * @return string The temp table name
     */
    protected function gatherBids(string $projectIdsTempTbl): string
    {
        if (empty($projectIdsTempTbl)) {
            throw new Exception('Missing projects temp table');
        }

        $this->info('Collecting Bid IDs...', 'v');
        $tempTable = "acct_prune_bids_temp_{$this->tempTableUid}";
        DB::statement(
            "
            CREATE TEMPORARY TABLE $tempTable
            (id INTEGER NOT NULL, PRIMARY KEY(id), INDEX compound_key (id, should_delete))
            SELECT 
                b.id, 
                IF(
                    b.deleted_at <= :daysPriorTs
                    OR pTemp.should_delete = true
                    OR pTemp.id IS NULL, -- handles bids pointing to deleted projects
                    true,
                    false
                ) as should_delete
            FROM {$this->tableNames->bid} AS b
            LEFT JOIN $projectIdsTempTbl AS pTemp ON pTemp.id = b.project_id
            WHERE b.account_id = :accountId
            ORDER BY 
                pTemp.should_delete DESC,
                b.deleted_at ASC, 
                b.id ASC
            ",
            ['accountId' => $this->opt->acctId, 'daysPriorTs' => $this->opt->daysPriorTs]
        );

        $this->deleteQueries->bid = DB::table("{$this->tableNames->bid} AS source")
            ->join("$tempTable AS bTemp", 'bTemp.id', '=', 'source.id')
            ->where('source.account_id', $this->opt->acctId)
            ->where('bTemp.should_delete', '=', true);

        $this->count('bid', DB::table("$tempTable AS source")->where('should_delete', '=', true));

        return $tempTable;
    }

    /**
     * Handle all the bid entities
     *
     * @param string $bidIdsTempTbl
     * @return void
     */
    protected function handleEntities(string $bidIdsTempTbl)
    {
        if (empty($bidIdsTempTbl)) {
            throw new Exception('Missing bids temp table');
        }

        foreach ($this->entityConfigs as $entityConfig) {
            if (!isset($entityConfig['entityClass'])) {
                continue;
            }

            $entityClass = $entityConfig['entityClass'];
            $inst = new $entityClass;

            // We can't perform cross DB engine joins so just handle nosql entities later
            if ($inst instanceof NoSqlEntity) {
                continue;
            }

            $entityTable = $inst->getTable();
            $this->info("Collecting IDs for $entityTable ...", 'v');
            $entityQuery = DB::table("$entityTable AS source")
                ->leftJoin("$bidIdsTempTbl AS bTemp", 'bTemp.id', '=', 'source.bid_id')
                ->where('source.account_id', $this->opt->acctId)
                ->where(function ($q) {
                    $q->where('bTemp.should_delete', '=', true); // handles entities pointing to deleted bids/projects
                    $q->orWhereNull('bTemp.id'); // handles entities pointing to non-existent bids
                });

            $this->deleteQueries->{$entityConfig['type']} = $entityQuery;
            $this->count($entityConfig['type'], $entityQuery);
        }
    }

    /**
     * Run a delete on a type using its stored delete query(s)
     *
     * @param array $entityConfig
     * @return void
     */
    protected function performDelete(array $entityConfig)
    {
        if (!isset($entityConfig['entityClass'])) {
            return;
        }

        $type = $entityConfig['type'];
        if (empty($this->deleteQueries->$type)) {
            return;
        }

        $entityClass = $entityConfig['entityClass'];
        $inst = new $entityClass;
        $table = $inst->getTable();

        $this->info(
            sprintf(
                'Deleting %d %s ...',
                $this->counts->$type,
                $this->getLabel($type, $this->counts->$type)
            ),
            'v'
        );

        // need to handle nosql entities differently
        if ($inst instanceof NoSqlEntity) {
            if (is_array($this->deleteQueries->$type)) {
                foreach ($this->deleteQueries->$type as $delQ) {
                    $delQ->forceDelete();
                }
            } else {
                $this->deleteQueries->$type->forceDelete();
            }

            return;
        }

        $this->deleteQueries->$type->delete();

        if ($this->counts->$type > 0 && $this->opt->shouldOptimize) {
            $this->info("Optimizing $table...", 'v');
            DB::statement("OPTIMIZE TABLE $table");
        }
    }

    /**
     * Gets the label in singular or plural form depending on the supplied count
     *
     * @param string $txt
     * @param integer $count
     * @return string
     */
    protected function getLabel(string $txt, int $count): string
    {
        return $count > 1
            ? $txt
            : str_plural($txt);
    }

    /**
     * Handle the NoSQL Bid records via batch
     *
     * @param array $bidIds
     * @return void
     */
    protected function handleNoSqlBids(array $bidIds)
    {
        $this->info('Collecting IDs for NoSQLBids...', 'v');

        // batch to avoid potential `where in` restrictions
        $count = 0;
        $ids = [];
        $chunks = array_chunk($bidIds, 20);
        foreach ($chunks as $chunk) {
            $q = Models\NoSQL\Bid::withTrashed()
                ->whereIn('source_bid_id', $chunk);

            if ($this->getOutput()->isVerbose()) {
                $count += $q->count();
            }

            if ($this->getOutput()->isVeryVerbose()) {
                $ids = array_merge(
                    $ids,
                    $q->get(['id'])
                        ->pluck('id')
                        ->toArray()
                );
            }

            $this->deleteQueries->noSqlBid[] = $q;
        }

        $this->counts->noSqlBid = $count;
        $this->info("Found $count NoSqlBids", 'v');

        $this->info(
            sprintf(
                'NoSqlBids to delete: %s',
                implode(', ', $ids)
            ),
            'vv'
        );
    }

    /**
     * Count the entites to be deleted
     * Expose details and ids according to verbosity levels
     *
     * @param string $table
     * @param QueryBuilder $query
     * @return void
     */
    protected function count(string $type, QueryBuilder $query)
    {
        $this->counts->$type = $query->count();

        $this->info(
            sprintf(
                'Found %d %s to delete',
                $this->counts->$type,
                $this->getLabel($type, $this->counts->$type)
            ),
            'v'
        );

        if ($this->getOutput()->isVeryVerbose()) {
            $ids = $this->getIds(
                $query
            );

            if (count($ids) > 0) {
                $this->info(
                    sprintf(
                        'IDs to delete for type `%s`: 
                        %s',
                        $type,
                        implode(', ', $ids)
                    )
                );
            }
        }
    }

    /**
     * Get all of the ids from the given query
     *
     * @param QueryBuilder $query
     * @return array
     */
    protected function getIds(QueryBuilder $query): array
    {
        return $query
            ->get(['source.id'])
            ->pluck('id')
            ->toArray();
    }
}
