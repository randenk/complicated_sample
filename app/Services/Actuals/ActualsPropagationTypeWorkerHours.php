<?php
namespace App\Services\Actuals;

class ActualsPropagationTypeWorkerHours extends ActualsPropagationTypeWorker
{
    public static $actualType = 'Hours';
    public static $actualProp = 'actual_hours';
    public static $actualConfProp = 'actual_hours_confidence_factor';
    public static $origProp = 'labor_hours';
}
