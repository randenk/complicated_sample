<?php
namespace App\Services\Actuals;

use App\Models;
use App\Services\Actuals\ActualsPropagationService as APS;

class ActualsPropagationTypeWorker
{
    public $worker;

    // aliases
    public $entity, $parentWorker, $parent, $parentTypeWorker;

    public $degreeOfSeparation;
    public $weight;
    public $manualStatus;
    public $manualActualValue;
    public $newActual;
    public $newConfidence;

    // defined in the extended classes
    public static $origProp, $actualType, $actualProp, $actualConfProp;

    // calculations
    public $siblingsManualActualsSum, $childrenManualActualsSum, $siblingsManualOrigSum;

    // cached entities
    private $manualComponentSiblings;

    public function __construct($worker)
    {
        $this->worker = $worker;

        // aliases
        $this->entity = $this->worker->entity;
        if (!empty($this->worker->parentWorker)) {
            $this->parentWorker = $this->worker->parentWorker;
            $this->parent = $this->parentWorker->entity;
            $this->log("Parent: ".$this->parent->id);

            if (!empty($this->parentWorker->getTypeWorker($this))) {
                $this->parentTypeWorker = $this->parentWorker->getTypeWorker($this);
            }
        }

        // ensure we check/set if actuals were manually set before we modify anything
        $this->setManualStatus();
    }

    private function log($msg)
    {
        \Log::debug($this->worker->context.' - '.$msg);
    }

    /**
     * Getter for the line item actual value placeholder
     * @return float|null
     */
    public function getNewActualValue()
    {
        return $this->newActual;
    }

    /**
     * Apply the placeholders to the entity
     */
    public function applyActuals()
    {
        $this->entity->{static::$actualProp} = $this->getNewActualValue();
        $this->entity->{static::$actualConfProp} = $this->getConfidence();
    }

    /**
     * Setter Line Item Actual Value (newValue)
     * This will placehold the value until we actually write it to the entity
     * @return float|bool
     */
    public function setLineItemActualValue()
    {
        if (!APS::isLineItem($this->worker->entity)) {
            return false;
        }

        if (isset($this->newActual)) {
            return $this->newActual;
        }

        if ($this->shouldWriteLineItemActualValue()) {
            // if no DoS is set, it means this should assume the original value
            if (empty($this->degreeOfSeparation)) {
                $this->log('No DoS was set, carry over the original value');
                $actualValue = $this->origProp();
            } else {
                // if this entity doesn't have siblings who have manual actuals,
                // go find the next manual actual and use that to propagate from
                $this->log("component sibs manual actual sum: ".$this->getComponentSiblingsManualActualsSum());
                $this->log("parent actual: ".$this->parentTypeWorker->getNewActualValue());
                $this->log("component siblings sum: ".$this->getComponentSiblingsManualActualsSum());

                $formula = $this->propagateFromNextManualActual();
                $actualValue = $formula['result'];
                $this->log('Formula: '.$formula['render']);
            }

            $this->newActual = $actualValue;

            $this->log("New Actual: ".$this->newActual);

            return $this->newActual;
        }

        // nothing happened
        return false;
    }

    /**
     * Traverses back up the structure to find all the parent's siblings
     * who have manual actuals set and finally,
     * to the (next level higher) parent which has a manual actual set
     * Then, multiplies all the weights of each entity with the found actual
     * @return array Details about the propagation formula including a render
     */
    private function propagateFromNextManualActual(): array
    {
        $render = '';
        if ($this->getManualStatus()) {
            $this->log('Finally found the manual actual value: '.$this->getNewActualValue());
            $result = $this->getNewActualValue();
            $render .= "{$this->getNewActualValue()}";
        } else {
            $this->log('Manual actual not found.');
            $next = $this->parentTypeWorker->propagateFromNextManualActual();
            $result = $this->getWeight() * ($next['result'] - $this->getComponentSiblingsManualActualsSum());
            $render .= "{$this->getWeight()} * ({$next['render']} - {$this->getComponentSiblingsManualActualsSum()})";
        }

        return [
            'result' => $result,
            'render' => $render
        ];
    }

    /**
     * Decision maker about whether the entity should be propagated from it's parent
     * @return bool
     */
    private function shouldWriteLineItemActualValue(): bool
    {
        // don't reassign top level components
        if ($this->parent === null) {
            $this->log('Not writing because no parent exists');
            return false;
        }

        // anything with a null value means it's never been set
        if ($this->getNewActualValue() === null) {
            $this->log('Writing because new actual is null');
            return true;
        }

        // has it already been propagated before? we should recalc
        if ($this->getConfidence() > 0) {
            $this->log('Writing because confidence is > 0');
            return true;
        }

        // all other cases
        return false;
    }

    /**
     * If we encounter a manual actual at any point, we need to emit that to its
     * parents and children so that they know they are influenced by this.
     * Unless, they are themselves set manually
     * @param int $increment=1 the current degree to increment on
     * @param string|null $direction The flow of the emanation as it traverses the structure
     */
    public function emanateDegreeOfSeparation($increment=1, $direction=null)
    {
        $this->degreeOfSeparation = $increment;
        $next = $this->degreeOfSeparation+1;

        $this->log(
            'Emitting a DoS of '.$this->degreeOfSeparation.' to '
            .get_class($this->entity).' ('.$this->entity->id.') - '.static::class
        );

        // handle parents
        if (($direction === 'up' || $direction === null)
            && !empty($this->parentTypeWorker)
            && !$this->parentTypeWorker->getManualStatus()
        ) {
            $this->parentTypeWorker->emanateDegreeOfSeparation($next, 'up');
        }

        // handle children
        if (($direction === 'down' || $direction === null)
            && $this->worker->getChildWorkers()->isNotEmpty()
        ) {
            foreach($this->worker->getChildWorkers() as $childWorker) {
                if (!empty($childWorker->getTypeWorker($this))
                    && !$childWorker->getTypeWorker($this)->getManualStatus()
                ) {
                    $childWorker->getTypeWorker($this)->emanateDegreeOfSeparation($next, 'down');
                }
            }
        }
    }

    /**
     * Get the confidence factor
     * @return int confidence factor
     */
    public function getConfidence(): int
    {
        return $this->newConfidence;
    }

    /**
     * Assign and return confidence factors based on conditions
     * @return int
     */
    public function setConfidence(): int
    {
        if (!isset($this->newConfidence)) {
            if ($this->getManualStatus()) {
                $confidence = 1;
                $this->log("Setting confidence to $confidence due to manual status");
            } else if (!empty($this->degreeOfSeparation)) {
                $confidence = (int) $this->degreeOfSeparation;
                $this->log("Setting confidence to ".$this->degreeOfSeparation." due to DoS");
            } else {
                // assume the original (Bid) value
                $confidence = 0;
            }

            $this->newConfidence = $confidence;
        }

        return $this->newConfidence;
    }

    /**
     * Set the placeholder actuals with the manual actual values
     */
    public function setManualActualValue()
    {
        if (!APS::isComponent($this->worker->entity)
            && !APS::isBid($this->worker->entity)
        ) {
            return false;
        }

        if ($this->getManualStatus()) {
            $this->newActual = $this->entity->{static::$actualProp};
            $this->newConfidence = 1;
            $this->log("Entity has manual actual value. Setting newActual ".$this->newActual);
            $this->log("Setting newConfidence ".$this->newConfidence);

            $this->log('Emanating DoS...');
            $this->emanateDegreeOfSeparation();
        }
    }

    /**
     * Set entity actuals which are calculated from it's children
     */
    public function setCalculatedActualValue()
    {
        if (!APS::isComponent($this->worker->entity)
            && !APS::isBid($this->worker->entity)
        ) {
            return false;
        }

        // don't try to reset it
        if (!empty($this->getNewActualValue())) {
            return false;
        }

        if ($this->worker->getChildWorkers()->isNotEmpty()) {
            $this->log("calc value has children ".$this->worker->getChildWorkers()->count());
            $actualValue = $this->worker->getChildWorkers()->sum(function($childWorker) {
                if (!empty($childWorker->getTypeWorker($this)->getNewActualValue())) {
                    $this->log("child ({$childWorker->entity->id}) new actual val: ".$childWorker->getTypeWorker($this)->getNewActualValue());
                    return $childWorker->getTypeWorker($this)->getNewActualValue();
                }
            });

            $this->newActual = (float) $actualValue;

            $this->log("Summing children for component actual. Setting: ".$this->getNewActualValue());
        }
    }

    /**
     * Setter for the actual from the bid (estimated) value
     */
    public function setActualFromBidValue()
    {
        if (!APS::isComponent($this->worker->entity)
            && !APS::isBid($this->worker->entity)
        ) {
            return false;
        }

        // don't try to reset it
        if (!empty($this->getNewActualValue())) {
            return false;
        }

        if (!$this->getManualStatus()) {
            $this->newActual = $this->origProp();
        }
    }

    /**
     * This entity's original value
     * @return float|null
     */
    public function origProp()
    {
        return $this->entity->{static::$origProp};
    }

    /**
     * The parent's original value
     * @return float|null
     */
    public function parentOrigProp()
    {
        if (isset($this->parent->{static::$origProp})) {
            return $this->parent->{static::$origProp};
        }

        return null;
    }

    /**
     * Getter for weight
     * @return float
     */
    public function getWeight(): float
    {
        if (!isset($this->weight)) {
            $this->setWeight();
        }

        return $this->weight;
    }

    /**
     * Determine how this entity falls in the distribution of it's parent
     * @return float
     */
    private function setWeight(): float
    {
        if (isset($this->weight)) {
            return $this->weight;
        }

        if ($this->getManualStatus()) {
            $weight = 1;
        } else if ($this->getManualComponentSiblings()->isNotEmpty()) {
            $this->log('Setting weight from manual components siblings');
            $divisor = ($this->parentOrigProp() - $this->getComponentSiblingsManualsOrigSum());

            $this->log("Weight Divisor: ".$divisor);
            $this->log("Orig ".static::$origProp.": ".$this->origProp());

            $weight = (float) ($divisor > 0)  // don't even think about dividing by zero!
                ? $this->origProp() / $divisor
                : 0;
        } else if ($this->parentOrigProp() <= 0) {
            $weight = (float) 0;
        } else {
            $weight = (float) $this->origProp() / $this->parentOrigProp();
        }

        $this->weight = $weight;

        $this->log("Weight: $this->weight");

        return $this->weight;
    }

    /**
     * Setter for the manualStatus and, if it is manual, set manualActualValue
     * @return bool
     */
    private function setManualStatus(): bool
    {
        if (!isset($this->manualStatus)) {
            $confidenceLooksManual = $this->entity->{static::$actualConfProp} === 1 || $this->entity->{static::$actualConfProp} === null;
            $this->log("Confidence looks manual: ". (int) $confidenceLooksManual);
            $valueIsNotNull = $this->entity->{static::$actualProp} !== null;
            $this->log("Value is not null: ". (int) $valueIsNotNull);
            $this->manualStatus = $confidenceLooksManual && $valueIsNotNull;
            $this->log("Setting Manual Status: ".(int) $this->manualStatus);
        }

        if ($this->manualStatus) {
            if (!isset($this->manualActualValue)) {
                $this->manualActualValue = (float) $this->entity->{static::$actualProp};
                $this->log("Setting Manual Actual Value: ".(int) $this->manualActualValue);
            }
        }

        return $this->manualStatus;
    }

    /**
     * Setter for manual actual value
     * @return float
     */
    public function getManualActualValue(): float
    {
        return $this->manualActualValue;
    }

    /**
     * Getter for manualStatus
     * @return bool
     */
    private function getManualStatus(): bool
    {
        return $this->manualStatus;
    }

    /**
     * Getter for component sibling's sum of originals
     * @return float
     */
    private function getComponentSiblingsManualsOrigSum(): float
    {
        if (!isset($this->siblingsManualOrigSum)) {
            if ($this->getManualComponentSiblings()->isNotEmpty()) {
                $this->siblingsManualOrigSum = $this->getManualComponentSiblings()->sum(function($worker){
                    return $worker->getTypeWorker($this)->origProp();
                });

                $this->log("Siblings Manuals Originals Sum: ".$this->siblingsManualOrigSum);
            }
        }

        return (float) $this->siblingsManualOrigSum;
    }

    /**
     * Getter for component sibling's sum of actuals which were manually set
     * @return float
     */
    private function getComponentSiblingsManualActualsSum(): float
    {
        if (!isset($this->siblingsManualActualsSum)) {
            if ($this->getManualComponentSiblings()->isNotEmpty()) {
                $this->siblingsManualActualsSum = $this->getManualComponentSiblings()->sum(function($worker) {
                    return $worker->getTypeWorker($this)->getManualActualValue();
                });
                $this->log("Siblings Manual Actuals Sum: ".$this->siblingsManualActualsSum);
            }
        }

        return (float) $this->siblingsManualActualsSum;
    }

    /**
     * Getter for sum of children manual actuals (will only be of components)
     * @return float
     */
    private function getChildrenManualActualsSum()
    {
        if (!isset($this->childrenManualActualsSum)) {
            if ($this->worker->getComponentChildWorkers()->isNotEmpty()) {
                $this->childrenManualActualsSum = $this->worker->getComponentChildWorkers()->filter(function($worker) {
                    return $worker->getTypeWorker($this)->getManualStatus();
                })->sum(function($worker) {
                    return $worker->getTypeWorker($this)->getManualActualValue();
                });
                $this->log("Component children manual actuals: ".$this->childrenManualActualsSum);
            }
        }

        return (float) $this->childrenManualActualsSum;
    }

    /**
     * Setter for manual component siblings
     * Filter the component siblings by those that have manual actuals set
     */
    private function setManualComponentSiblings()
    {
        if (!isset($this->manualComponentSiblings)) {
            $this->log("component siblings workers: ".$this->worker->getComponentSiblingWorkers()->count());
            if ($this->worker->getComponentSiblingWorkers()->isNotEmpty()) {
                $this->log("component siblings count ".$this->worker->getComponentSiblingWorkers()->count());
                $this->manualComponentSiblings = $this->worker->getComponentSiblingWorkers()->filter(function($worker) {
                    return $worker->getTypeWorker($this)->getManualStatus();
                });

                $this->log('Found '.$this->manualComponentSiblings->count().' manual component siblings.');
            }
        }
    }

    /**
     * Getter for manual component siblings
     * @return \Illuminate\Support\Collection Collection of filtered siblings
     */
    public function getManualComponentSiblings(): \Illuminate\Support\Collection
    {
        if (!isset($this->manualComponentSiblings)) {
            $this->setManualComponentSiblings();
        }
        return $this->manualComponentSiblings ?? collect([]);
    }

    /**
     * Render this class to a string
     * @return string
     */
    public function __toString(): string
    {
        $out = [
            'Type: '.static::$actualType,
            'Original Value: '.$this->origProp(),
            'Actual Value: '.$this->entity->{static::$actualProp},
            'Actual Confidence: '.$this->entity->{static::$actualConfProp},
            'Degree of separation: '.$this->degreeOfSeparation,
            'Weight: '.$this->weight,
            'Is Manual?: '.(int)$this->manualStatus,
        ];
        return implode("\n", $out);
    }
}
