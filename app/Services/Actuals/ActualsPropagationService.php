<?php

namespace App\Services\Actuals;

use Cache;
use App\Models;
use Carbon\Carbon;
use App\Traits\MetricsTrait;
use \Illuminate\Support\Collection;

/*
 * A service that will distribute Component actuals into "perceived" values on the Line Items
 */

class ActualsPropagationService
{
    use MetricsTrait;

    /**
     * Bid container
     *
     * @var Models\Bid
     */
    private $bid;

    /**
     * Bid ID
     *
     * @var int
     */
    private static $bidId;

    /**
     * Component Group container
     *
     * @var Models\ComponentGroup
     */
    private static $componentGroup;

    /**
     * Cache expiration time (in minutes)
     *
     * @var int
     */
    public static $cacheExpiryMinutes = 5;

    /**
     * Cache key
     *
     * @var string
     */
    public static $cacheKey = 'propagation_service_entity_storage';

    /**
     * Type Worker Class definitions
     * we need to map types because the value props don't match the actuals props :/
     *
     * @var array
     */
    public static $typeWorkerClasses = [
        ActualsPropagationTypeWorkerCost::class,
        ActualsPropagationTypeWorkerHours::class,
    ];

    /**
     * @param \App\Models\Bid               $bid
     * @param \App\Models\BidComponentGroup $componentGroup
     */
    public function __construct(Models\Bid $bid, Models\BidComponentGroup $componentGroup)
    {
        $this->bid = $bid;
        self::$bidId = $bid->id;
        self::$componentGroup = $componentGroup;

        \Log::debug("*****************************");
        \Log::debug("*** Begin new propagation ***");
        \Log::debug("*****************************");
    }

    /**
     * A method to kick things off using whatever top level entity we need
     * to build and evaluate a structure from
     * @param \App\Models\Bid|\App\Models\BidComponent $entity
     * @param \App\Models\Bid|\App\Models\BidComponent $parentWorker
     * @param bool $shouldSave=false
     * @throws \Exception
     */
    public function propagate($entity, $parentWorker = null, bool $shouldSave = false)
    {
        $worker = new ActualsPropagationWorker($entity, $parentWorker);
        $worker->run($shouldSave);

        \Log::debug($worker);

        $this->endMetricsTimer();
    }

    /**
     * Gathers and stores relevant entities from/to cache
     * @param  \App\Models\Bid|\App\Models\BidComponent $entity
     * @return Collection containing child collections of line items and components
     */
    public static function getChildren($entity): Collection
    {
        $children = collect([]);

        // find and add any children
        if (!empty($entity->config->line_items)) {
            \Log::debug('Finding line items...');
            $lineItems = self::getLineItems($entity->config->line_items);
            if ($lineItems->isNotEmpty()) {
                $children = $children->merge(self::cacheEntities($lineItems));
            }
        }

        if (!empty($entity->config->components)) {
            \Log::debug('Finding child components...');
            $components = self::getComponents($entity->config->components);
            if ($components->isNotEmpty()) {
                $children = $children->merge(self::cacheEntities($components));
            }
        }

        if (self::isBid($entity)) {
            \Log::debug('Finding components of bid...');
            $components = self::getComponentsOfBid();

            if ($components->isNotEmpty()) {
                $children = $children->merge(self::cacheEntities($components));
            }
        }

        return $children;
    }

    /**
     * Getter for cached entities
     * @return Collection [description]
     */
    private static function getAllCachedEntities(): Collection
    {
        return Cache::get(self::$cacheKey, collect([
            'App\Models\Bid' => collect([]),
            'App\Models\BidComponent' => collect([]),
            'App\Models\BidLineItem' => collect([]),
        ]));
    }

    /**
     * Adds entities to the cache in the appropriate organization
     * if entities already exist, it will append them
     * @param  Collection $entities [description]
     * @return Collection
     */
    private static function cacheEntities(Collection $entities): Collection
    {
        $cachedEntities = self::getAllCachedEntities();

        foreach ($entities as $entity) {
            $entityClass = get_class($entity);
            \Log::debug("Caching $entityClass ({$entity->id})");

            $cachedEntities->get($entityClass)->put($entity->id, $entity);
        }

        self::putCache($cachedEntities);
        return $entities;
    }

    /**
     * Adds an entity into the cache in it's appropriate organization
     * @param \App\Models\Bid|\App\Models\BidComponent|\App\Models\BidLineItem $entity
     */
    public static function cacheEntity($entity)
    {
        $cachedEntities = self::getAllCachedEntities();
        $entityClass = get_class($entity);
        \Log::debug("Caching $entityClass ({$entity->id})");

        $cachedEntities->get($entityClass)->put($entity->id, $entity);

        self::putCache($cachedEntities);
    }

    /**
     * Getter for an entity from the cache
     * @param string $typeClass
     * @param int $id
     * @return \App\Models\Bid|\App\Models\BidComponent|\App\Models\BidLineItem
     */
    public static function getEntity(string $typeClass, int $id)
    {
        return self::getAllCachedEntities()->get($typeClass)->get($id);
    }

    /**
     * Gets line items by their ids
     * @param array $ids
     * @return Collection
     */
    private static function getLineItems(array $ids): Collection
    {
        $entities = self::getAllCachedEntities()
            ->get('App\Models\BidLineItem')
            ->whereIn('id', $ids)
            ->where('is_included', '=', 1)
            ->where('is_active', '=', 1);

        if ($entities->isNotEmpty()) {
            return $entities;
        }

        // get them en mass to avoid querying each separately
        return Models\BidLineItem::whereIn('id', $ids)
            ->where('is_included', '=', 1)
            ->where('is_active', '=', 1)
            ->get()
            ->keyBy('id');
    }

    /**
     * Gets components by their ids
     * @param array $ids
     * @return Collection
     */
    private static function getComponents(array $ids): Collection
    {
        $entities = self::getAllCachedEntities()
            ->get('App\Models\BidComponent')
            ->whereIn('id', $ids)
            ->where('component_group_id', '=', self::$componentGroup->id)
            ->where('bid_id', '=', self::$bidId)
            ->keyBy('id');

        if ($entities->isNotEmpty()) {
            return $entities;
        }

        // get them en mass to avoid querying each separately
        return Models\BidComponent::where('component_group_id', '=', self::$componentGroup->id)
            ->whereIn('id', $ids)
            ->where('bid_id', '=', self::$bidId)
            ->get()
            ->keyBy('id');
    }

    /**
     * Getter for components of a bid
     * @return Collection
     */
    public static function getComponentsOfBid(): Collection
    {
        $entities = self::getAllCachedEntities()
            ->get('App\Models\BidComponent')
            ->where('component_group_id', '=', self::$componentGroup->id)
            ->where('bid_id', '=', self::$bidId)
            ->where('config.is_nested', '=', false)
            ->keyBy('id');

        if ($entities->isNotEmpty()) {
            return $entities;
        }

        // get them en mass to avoid querying each separately
        return Models\BidComponent::where('component_group_id', '=', self::$componentGroup->id)
            ->where('bid_id', '=', self::$bidId)
            ->where('config->is_nested', '=', false)
            ->get()
            ->keyBy('id');
    }

    /**
     * Writes the contents into the cache
     * @param  Collection $contents
     */
    private static function putCache(Collection $contents)
    {
        $expiresAt = Carbon::now()->addMinutes(self::$cacheExpiryMinutes);
        Cache::put(self::$cacheKey, $contents, $expiresAt);
    }

    /**
     * Reset the existing actuals and confidence factors to ensure a fresh start
     */
    public function resetExistingActuals()
    {
        $componentsQuery = Models\BidComponent::where('bid_id', '=', self::$bidId)
            ->where('component_group_id', '=', self::$componentGroup->id);
        $lineItemsQuery = Models\BidLineItem::where('bid_id', '=', self::$bidId);

        foreach (self::$typeWorkerClasses as $typeWorkerClass) {
            $this->bid->update([
                $typeWorkerClass::$actualProp => null,
                $typeWorkerClass::$actualConfProp => null,
            ]);
            $componentsQuery->update([
                $typeWorkerClass::$actualProp => null,
                $typeWorkerClass::$actualConfProp => null,
            ]);
            $lineItemsQuery->update([
                $typeWorkerClass::$actualProp => null,
                $typeWorkerClass::$actualConfProp => null,
            ]);
        }

        Cache::forget(self::$cacheKey);
    }

    /**
     * Is the entity a Bid?
     * @param object $entity
     * @return bool
     */
    public static function isBid($entity): bool
    {
        return $entity instanceof Models\Bid;
    }

    /**
     * Is the entity a component?
     * @param object $entity
     * @return bool
     */
    public static function isComponent($entity): bool
    {
        return $entity instanceof Models\BidComponent;
    }

    /**
     * Is the entity a line item?
     * @param object $entity
     * @return bool
     */
    public static function isLineItem($entity): bool
    {
        return $entity instanceof Models\BidLineItem;
    }
}
