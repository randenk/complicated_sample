<?php
namespace App\Services\Actuals;

class ActualsPropagationTypeWorkerCost extends ActualsPropagationTypeWorker
{
    public static $actualType = 'Cost';
    public static $actualProp = 'actual_cost';
    public static $actualConfProp = 'actual_cost_confidence_factor';
    public static $origProp = 'cost';
}
