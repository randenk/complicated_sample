<?php

namespace App\Services\Actuals;

use App\Models;
use App\Services\Actuals\ActualsPropagationService as APS;

// Worker example structure:
// entity
// parentWorker
// childWorkers
// siblingWorkers
// types
//  - cost
//      - original
//      - actual
//      - confidence
//      - manualStatus
//      - degreeOfSeparation
//      - weight
//  - ... other types
class ActualsPropagationWorker
{
    // general structure organization
    public $entity;
    public $parentWorker;
    public $siblingWorkers;
    public $childWorkers;
    public $typeWorkers;

    // specifics
    private $componentSiblingWorkers;
    private $componentChildWorkers;

    public $context;
    public $shouldSave;

    /**
     * Recursive function to propagate a single entity's actuals
     * down to it's lowest level line item while accounting for any other actuals
     * encountered along the way
     *
     * @param  \App\Models\Bid|\App\Models\BidComponent|\App\Models\BidLineItem  $entity
     * @param  \App\Models\Bid|\App\Models\BidComponent|\App\Models\BidLineItem  $parentWorker
     * @throws \Exception
     */
    public function __construct($entity, $parentWorker=null)
    {
        $this->entity = $entity;
        $this->parentWorker = $parentWorker;

        \Log::debug("-----------------------------");
        $this->context = get_class($this->entity)." (".$this->entity->id.")";
        APS::cacheEntity($this->entity);

        $this->log("Creating types & children ...");

        $this->setTypeWorkers();
        $this->setChildWorkers();
    }

    private function log($msg)
    {
        \Log::debug($this->context.' - '.$msg);
    }

    /**
     * The main assignActuals method that will handle the validation and propagation
     * @param bool $shouldSave=null
     * @throws \Exception
     */
    public function run(bool $shouldSave=null)
    {
        $this->log("Running...");
        $this->shouldSave = $shouldSave;

        $this->log("Creating siblings and component focused groups ...");
        $this->setSiblingWorkers();
        $this->setComponentSiblingWorkers();
        $this->setComponentChildWorkers();

        if (empty($this->typeWorkers)) {
            throw new \Exception("Missing Type Workers.");
        }

        // evaluate and set each type separately
        foreach ($this->typeWorkers as $typeWorker) {
            $typeWorker->setManualActualValue();
        }

        // recurse into children
        if (!empty($this->childWorkers)) {
            foreach ($this->childWorkers as $childWorker) {
                $childWorker->run($shouldSave);
            }
        }

        // run post actions
        // these are dependent on the line items all being set first
        $this->log("Running post actions...");
        foreach ($this->typeWorkers as $typeWorker) {
            $typeWorker->setCalculatedActualValue();
            $typeWorker->setActualFromBidValue();
            $typeWorker->setLineItemActualValue();
            $typeWorker->setConfidence();
            $typeWorker->applyActuals();
        }

        if ($shouldSave) {
            $this->log('Saving entity.');
            $this->entity->save();
        }
    }

    /**
     * Save the entity
     */
    public function save()
    {
        $this->entity->save();
    }

    /**
     * Validates the entites
     * @throws \Exception
     * @return bool
     */
    private function validateEntities(): bool
    {
        if (!APS::isBid($this->entity)
            && !APS::isComponent($this->entity)
            && !APS::isLineItem($this->entity)
        ) {
            throw new \Exception(
                get_class($this->entity)
                .' Entity is not an instance of these types:'
                .'('.Models\BidComponent::class
                .','.Models\BidLineItem::class
                .','.Models\Bid::class.')'
            );
        }

        $this->log("Validated");
        return true;
    }

    /**
     * Setter for Type Workers
     */
    private function setTypeWorkers()
    {
        if (!isset($this->typeWorkers)) {
            if (empty(APS::$typeWorkerClasses)) {
                throw new \Exception('Missing Type Worker classes.');
            }

            $this->typeWorkers = collect([]);

            foreach (APS::$typeWorkerClasses as $typeClass) {
                $this->validateEntities();
                // assemble the class name for the appropriate actuals type
                $this->typeWorkers->push(new $typeClass($this));
            }
        }
    }

    /**
     * Getter for Type Workers
     * @return \Illuminate\Support\Collection
     */
    public function getTypeWorkers(): \Illuminate\Support\Collection
    {
        return $this->typeWorkers ?? collect([]);
    }

    /**
     * Getter for a matching typeWorker
     * @param  \App\Services\Actuals\ActualsPropagationTypeWorker $callingTypeWorker the instance looking for the match
     * @return \App\Services\Actuals\ActualsPropagationTypeWorker
     */
    public function getTypeWorker($callingTypeWorker)
    {
        $callingTypeWorkerClass = get_class($callingTypeWorker);
        return $this->getTypeWorkers()->first(function($typeWorker) use ($callingTypeWorkerClass) {
            return $typeWorker instanceof $callingTypeWorkerClass;
        });
    }

    /**
     * Traverse deeper into the component structure
     */
    private function setChildWorkers()
    {
        if (!isset($this->childWorkers)) {
            $this->childWorkers = collect([]);

            // only components have nested entities
            if (APS::isBid($this->entity) || APS::isComponent($this->entity)) {
                $this->log("Finding children...");
                $children = APS::getChildren($this->entity);
                $this->log("Found ".$children->count()." children");

                foreach ($children as $type => $childEntity) {
                    $childWorker = new ActualsPropagationWorker($childEntity, $this);
                    $this->childWorkers->push($childWorker);
                }
            }
            $this->log("child workers: ".$this->childWorkers->keys());
        }
    }

    /**
     * Getter for Child Workers
     * @return \Illuminate\Support\Collection
     */
    public function getChildWorkers(): \Illuminate\Support\Collection
    {
        return $this->childWorkers ?? collect([]);
    }

    /**
     * Setter for Sibling Workers
     */
    private function setSiblingWorkers()
    {
        if (!isset($this->siblingWorkers)) {
            if (isset($this->parentWorker) && $this->parentWorker->getChildWorkers()->isNotEmpty()) {
                $this->siblingWorkers = $this->parentWorker->getChildWorkers()->filter(function($childWorker) {
                    // exclude itself
                    return $childWorker->entity->id !== $this->entity->id;
                });
                $this->log('Found '.$this->siblingWorkers->count().' sibling workers.');
            }
        }
    }

    /**
     * Getter for Sibling workers
     * @return \Illuminate\Support\Collection
     */
    public function getSiblingWorkers(): \Illuminate\Support\Collection
    {
        return $this->siblingWorkers ?? collect([]);
    }

    /**
     * Setter for Component Sibling workers
     */
    private function setComponentSiblingWorkers()
    {
        if (!isset($this->componentSiblingWorkers)) {
            $this->log("sibling workers: ".$this->getSiblingWorkers()->count());
            if ($this->getSiblingWorkers()->isNotEmpty()) {
                $this->componentSiblingWorkers = $this->getSiblingWorkers()->filter(function($siblingWorker) {
                    return APS::isComponent($siblingWorker->entity);
                });
                $this->log('Found '.$this->componentSiblingWorkers->count().' component sibling workers.');
            }
        }
    }

    /**
     * Getter for Component Sibling Workers
     * @return \Illuminate\Support\Collection
     */
    public function getComponentSiblingWorkers(): \Illuminate\Support\Collection
    {
        return $this->componentSiblingWorkers ?? collect([]);
    }

    /**
     * Setter for Component Child Workers
     */
    private function setComponentChildWorkers()
    {
        if (!isset($this->componentChildWorkers)) {
            if ($this->getChildWorkers()->isNotEmpty()) {
                $this->componentChildWorkers = $this->getChildWorkers()->filter(function($sibling) {
                    return APS::isComponent($sibling->entity);
                });
                $this->log('Found '.$this->componentChildWorkers->count().' child workers.');
            }
        }
    }

    /**
     * Getter for Component Child Workers
     * @return \Illuminate\Support\Collection
     */
    public function getComponentChildWorkers(): \Illuminate\Support\Collection
    {
        return $this->componentChildWorkers ?? collect([]);
    }

    /**
     * Render this class to a string
     * @return string
     */
    public function __toString(): string
    {
        $out = [
            '---------------------------',
            get_class($this->entity).' ('.$this->entity->id.')',
            (!empty($this->entity->title) ? $this->entity->title : null),
        ];
        foreach ($this->getTypeWorkers() as $typeWorker) {
            $out[] = $typeWorker;
        }
        foreach ($this->getChildWorkers() as $childWorker) {
            $out[] = $childWorker;
        }

        return implode("\n", array_filter($out));
    }
}
