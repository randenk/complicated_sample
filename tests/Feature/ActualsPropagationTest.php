<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models;
use Cache;
use Carbon\Carbon;
use App\Services\Actuals\ActualsPropagationService;

class ActualsPropagationTest extends TestCase
{
    private static $bid;
    private static $componentGroup;
    private static $svc;

    public function setUp()
    {
        parent::setUp();

        // create an arbitrary bid
        if (self::$bid === null) {
            self::$bid = factory(Models\Bid::class)->make([
                'id' => 1
            ]);
        }

        if (self::$componentGroup === null) {
            self::$componentGroup = factory(Models\BidComponentGroup::class)->make(['id' => 1, 'title' => 'Cost Codes']);
        }

        if (self::$svc === null) {
            self::$svc = new ActualsPropagationService(self::$bid, self::$componentGroup);
        }
    }

    // Bid Structure diagram
    //                  Bid
    //             ______|_______
    //           c1              c6
    //          __|_______       |
    //         /     |    \     l10
    //        c3     l1   c2
    //    ___/_____         \_____
    //   /    /  |  \        /  |  \
    // c5   c4   l5  l6     l2  l3  l4
    // |    _/_
    // |   |   \
    // l9  l7  l8
    public static function simpleBidStructure()
    {
        $sourceData['App\Models\Bid'] = collect([
            1 => factory(Models\Bid::class)->make([
                'id' => 1,
                'cost' => (double) 11700.00,
                'labor_hours' => (double) 120,
            ])
        ]);

        $sourceData['App\Models\BidComponent'] = collect([
            1 => factory(Models\BidComponent::class)->make([
                'id' => 1,
                'bid_id' => 1,
                'cost' => (double) 10700.00,
                'labor_hours' => (double) 120,
                'is_nested' => false,
                'component_group_id' => 1,
                'config' => (object) [
                    'components' => [2, 3],
                    'line_items' => [1]
                ]
            ]),
            2 => factory(Models\BidComponent::class)->make([
                'id' => 2,
                'bid_id' => 1,
                'cost' => (double) 1000,
                'labor_hours' => (double) 100,
                'component_group_id' => 1,
                'config' => (object) [
                    'is_nested' => true,
                    'line_items' => [2, 3, 4]
                ]
            ]),
            3 => factory(Models\BidComponent::class)->make([
                'id' => 3,
                'bid_id' => 1,
                'cost' => (double) 9600,
                'labor_hours' => (double) 0,
                'component_group_id' => 1,
                'config' => (object) [
                    'is_nested' => true,
                    'components' => [4, 5],
                    'line_items' => [5, 6]
                ]
            ]),
            4 => factory(Models\BidComponent::class)->make([
                'id' => 4,
                'bid_id' => 1,
                'cost' => (double) 5500,
                'labor_hours' => (double) 0,
                'component_group_id' => 1,
                'config' => (object) [
                    'is_nested' => true,
                    'line_items' => [7, 8]
                ]
            ]),
            5 => factory(Models\BidComponent::class)->make([
                'id' => 5,
                'bid_id' => 1,
                'cost' => (double) 2000,
                'labor_hours' => (double) 0,
                'component_group_id' => 1,
                'config' => (object) [
                    'is_nested' => true,
                    'line_items' => [9]
                ]
            ]),
            6 => factory(Models\BidComponent::class)->make([
                'id' => 6,
                'bid_id' => 1,
                'cost' => (double) 1000,
                'labor_hours' => (double) 0,
                'component_group_id' => 1,
                'config' => (object) [
                    'is_nested' => false,
                    'line_items' => [10]
                ]
            ])
        ]);

        $sourceData['App\Models\BidLineItem'] = collect([
            1 => factory(Models\BidLineItem::class)->make([
                'id' => 1,
                'bid_id' => 1,
                'cost' => (double) 100,
                'labor_hours' => (double) 20,
            ]),
            2 => factory(Models\BidLineItem::class)->make([
                'id' => 2,
                'bid_id' => 1,
                'cost' => (double) 200,
                'labor_hours' => (double) 100,
            ]),
            3 => factory(Models\BidLineItem::class)->make([
                'id' => 3,
                'bid_id' => 1,
                'cost' => (double) 300,
                'labor_hours' => (double) 0,
            ]),
            4 => factory(Models\BidLineItem::class)->make([
                'id' => 4,
                'bid_id' => 1,
                'cost' => (double) 500,
                'labor_hours' => (double) 0,
            ]),
            5 => factory(Models\BidLineItem::class)->make([
                'id' => 5,
                'bid_id' => 1,
                'cost' => (double) 800,
                'labor_hours' => (double) 0,
            ]),
            6 => factory(Models\BidLineItem::class)->make([
                'id' => 6,
                'bid_id' => 1,
                'cost' => (double) 1300,
                'labor_hours' => (double) 0,
            ]),
            7 => factory(Models\BidLineItem::class)->make([
                'id' => 7,
                'bid_id' => 1,
                'cost' => (double) 2100,
                'labor_hours' => (double) 0,
            ]),
            8 => factory(Models\BidLineItem::class)->make([
                'id' => 8,
                'bid_id' => 1,
                'cost' => (double) 3400,
                'labor_hours' => (double) 0,
            ]),
            9 => factory(Models\BidLineItem::class)->make([
                'id' => 9,
                'bid_id' => 1,
                'cost' => (double) 2000,
                'labor_hours' => (double) 0,
            ]),
            10 => factory(Models\BidLineItem::class)->make([
                'id' => 10,
                'bid_id' => 1,
                'cost' => (double) 1000,
                'labor_hours' => (double) 0,
            ]),
        ]);

        return collect($sourceData);
    }

    public function tearDown()
    {
        Cache::forget(ActualsPropagationService::$cacheKey);
    }

    private static function resetAllActuals($structure)
    {
        foreach ($structure as $structureType => $entities) {
            foreach ($entities as $entity) {
                $entity->actual_cost = null;
                $entity->actual_cost_confidence_factor = null;
                $entity->actual_hours = null;
                $entity->actual_hours_confidence_factor = null;
            }
        }
    }

    private function cacheStructure($structure)
    {
        $structureCollection = collect($structure);

        // mimic the cost into the hours values since we don't have established data for testing
        foreach ($structureCollection as $structureTypeEntities) {
            foreach ($structureTypeEntities as $id => $entity) {
                $entity->labor_hours = $entity->cost;
            }
        }

        Cache::put(ActualsPropagationService::$cacheKey, $structureCollection, Carbon::now()->addMinutes(5));
    }

    private function assertStructure($structure, $sourceData)
    {
        // defer to the simple structure as the source if none given
        if (empty($sourceData)) {
            $sourceData = self::simpleBidStructure();
        }

        foreach ($structure as $type => $entity) {
            foreach ($entity as $id => $testData) {
                // var_dump([round((double) $sourceData->get($type)->get($id)->actual_cost, 2), (double) $testData['actual']]);
                $this->assertTrue(round((double) $sourceData->get($type)->get($id)->actual_cost, 2) === (double) $testData['actual']);
                // var_dump([$sourceData->get($type)->get($id)->actual_cost_confidence_factor, $testData['confidence']]);
                $this->assertTrue($sourceData->get($type)->get($id)->actual_cost_confidence_factor === $testData['confidence']);

                // var_dump([round((double) $sourceData->get($type)->get($id)->actual_hours, 2), (double) $testData['actual']]);
                $this->assertTrue(round((double) $sourceData->get($type)->get($id)->actual_hours, 2) === (double) $testData['actual']);
                // var_dump([$sourceData->get($type)->get($id)->actual_hours_confidence_factor, $testData['confidence']]);
                $this->assertTrue($sourceData->get($type)->get($id)->actual_hours_confidence_factor === $testData['confidence']);
            }
        }
    }

    private function setStructureActuals($structure, $sourceData = null)
    {
        // defer to the simple structure as the source if none given
        if (empty($sourceData)) {
            $sourceData = self::simpleBidStructure();
        }

        self::resetAllActuals($sourceData);

        foreach ($structure as $type => $entities) {
            foreach ($entities as $id => $testData) {
                $sourceData->get($type)->get($id)->actual_cost = $testData['actual'];
                $sourceData->get($type)->get($id)->actual_cost_confidence_factor = $testData['confidence'];
                $sourceData->get($type)->get($id)->actual_hours = $testData['actual'];
                $sourceData->get($type)->get($id)->actual_hours_confidence_factor = $testData['confidence'];
            }
        }

        $this->cacheStructure($sourceData);

        return $sourceData;
    }

    public function testOnlyOneTopLevelComponentHasManualActual()
    {
        $modifiedData = $this->setStructureActuals([
            'App\Models\BidComponent' => [
                1 => ['actual' => 2100, 'confidence' => null],
            ]
        ]);

        self::$svc->propagate($modifiedData->get('App\Models\Bid')->get(1));

        $this->assertStructure([
            'App\Models\Bid' => [
                1 => ['actual' => 3100, 'confidence' => 2]
            ],
            'App\Models\BidComponent' => [
                1 => ['actual' => 2100, 'confidence' => 1],
                2 => ['actual' => 196.26, 'confidence' => 2],
                3 => ['actual' => 1884.11, 'confidence' => 2],
                4 => ['actual' => 1079.44, 'confidence' => 3],
                5 => ['actual' => 392.52, 'confidence' => 3],
                5 => ['actual' => 392.52, 'confidence' => 3],
                6 => ['actual' => 1000, 'confidence' => 0],
            ],
            'App\Models\BidLineItem' => [
                1 => ['actual' => 19.63, 'confidence' => 2],
                2 => ['actual' => 39.25, 'confidence' => 3],
                3 => ['actual' => 58.88, 'confidence' => 3],
                4 => ['actual' => 98.13, 'confidence' => 3],
                5 => ['actual' => 157.01, 'confidence' => 3],
                6 => ['actual' => 255.14, 'confidence' => 3],
                7 => ['actual' => 412.15, 'confidence' => 4],
                8 => ['actual' => 667.29, 'confidence' => 4],
                9 => ['actual' => 392.52, 'confidence' => 4],
                10 => ['actual' => 1000, 'confidence' => 0],
            ]
        ], $modifiedData);
    }

    public function testMultipleSubComponentsHaveManualActuals()
    {
        $modifiedData = $this->setStructureActuals([
            'App\Models\BidComponent' => [
                2 => ['actual' => 1500, 'confidence' => null],
                3 => ['actual' => 600, 'confidence' => null],
            ]
        ]);

        self::$svc->propagate($modifiedData->get('App\Models\Bid')->get(1));

        $this->assertStructure([
            'App\Models\Bid' => [
                1 => ['actual' => 3200, 'confidence' => 3]
            ],
            'App\Models\BidComponent' => [
                1 => ['actual' => 2200, 'confidence' => 2],
                2 => ['actual' => 1500, 'confidence' => 1],
                3 => ['actual' => 600, 'confidence' => 1],
                4 => ['actual' => 343.75, 'confidence' => 2],
                5 => ['actual' => 125, 'confidence' => 2],
                6 => ['actual' => 1000, 'confidence' => 0],
            ],
            'App\Models\BidLineItem' => [
                1 => ['actual' => 100, 'confidence' => 0],
                2 => ['actual' => 300, 'confidence' => 2],
                3 => ['actual' => 450, 'confidence' => 2],
                4 => ['actual' => 750, 'confidence' => 2],
                5 => ['actual' => 50, 'confidence' => 2],
                6 => ['actual' => 81.25, 'confidence' => 2],
                7 => ['actual' => 131.25, 'confidence' => 3],
                8 => ['actual' => 212.50, 'confidence' => 3],
                9 => ['actual' => 125, 'confidence' => 3],
                10 => ['actual' => 1000, 'confidence' => 0],
            ]
        ], $modifiedData);
    }

    public function testOnlyOneSubComponentHasManualActual()
    {
        $modifiedData = $this->setStructureActuals([
            'App\Models\BidComponent' => [
                2 => ['actual' => 700, 'confidence' => null],
            ]
        ]);

        self::$svc->propagate($modifiedData->get('App\Models\Bid')->get(1));

        $this->assertStructure([
            'App\Models\Bid' => [
                1 => ['actual' => 11400, 'confidence' => 3]
            ],
            'App\Models\BidComponent' => [
                1 => ['actual' => 10400, 'confidence' => 2],
                2 => ['actual' => 700, 'confidence' => 1],
                3 => ['actual' => 9600.00, 'confidence' => 0],
                4 => ['actual' => 5500.00, 'confidence' => 0],
                5 => ['actual' => 2000, 'confidence' => 0],
                6 => ['actual' => 1000, 'confidence' => 0],
            ],
            'App\Models\BidLineItem' => [
                1 => ['actual' => 100, 'confidence' => 0],
                2 => ['actual' => 140, 'confidence' => 2],
                3 => ['actual' => 210, 'confidence' => 2],
                4 => ['actual' => 350, 'confidence' => 2],
                5 => ['actual' => 800, 'confidence' => 0],
                6 => ['actual' => 1300, 'confidence' => 0],
                7 => ['actual' => 2100, 'confidence' => 0],
                8 => ['actual' => 3400, 'confidence' => 0],
                9 => ['actual' => 2000, 'confidence' => 0],
                10 => ['actual' => 1000, 'confidence' => 0],
            ]
        ], $modifiedData);
    }

    public function testMixOfComponentLevelsHaveManualActuals()
    {
        $modifiedData = $this->setStructureActuals([
            'App\Models\BidComponent' => [
                1 => ['actual' => 2100, 'confidence' => null],
                2 => ['actual' => 600, 'confidence' => null],
                3 => ['actual' => 1200, 'confidence' => null],
            ]
        ]);

        self::$svc->propagate($modifiedData->get('App\Models\Bid')->get(1));

        $this->assertStructure([
            'App\Models\Bid' => [
                1 => ['actual' => 3100, 'confidence' => 2]
            ],
            'App\Models\BidComponent' => [
                1 => ['actual' => 2100, 'confidence' => 1],
                2 => ['actual' => 600, 'confidence' => 1],
                3 => ['actual' => 1200, 'confidence' => 1],
                4 => ['actual' => 687.5, 'confidence' => 2],
                5 => ['actual' => 250, 'confidence' => 2],
                6 => ['actual' => 1000, 'confidence' => 0],
            ],
            'App\Models\BidLineItem' => [
                1 => ['actual' => 300, 'confidence' => 2],
                2 => ['actual' => 120, 'confidence' => 2],
                3 => ['actual' => 180, 'confidence' => 2],
                4 => ['actual' => 300, 'confidence' => 2],
                5 => ['actual' => 100, 'confidence' => 2],
                6 => ['actual' => 162.5, 'confidence' => 2],
                7 => ['actual' => 262.5, 'confidence' => 3],
                8 => ['actual' => 425, 'confidence' => 3],
                9 => ['actual' => 250, 'confidence' => 3],
                10 => ['actual' => 1000, 'confidence' => 0],
            ]
        ], $modifiedData);
    }

    public function testDifferenceBetweenComponentAndSubComponent()
    {
        $modifiedData = $this->setStructureActuals([
            'App\Models\BidComponent' => [
                1 => ['actual' => 5500, 'confidence' => null],
                2 => ['actual' => 700, 'confidence' => null],
                3 => ['actual' => 4400, 'confidence' => null],
                4 => ['actual' => 1900, 'confidence' => null],
            ]
        ]);

        self::$svc->propagate($modifiedData->get('App\Models\Bid')->get(1));

        $this->assertStructure([
            'App\Models\Bid' => [
                1 => ['actual' => 6500, 'confidence' => 2]
            ],
            'App\Models\BidComponent' => [
                1 => ['actual' => 5500, 'confidence' => 1],
                2 => ['actual' => 700, 'confidence' => 1],
                3 => ['actual' => 4400, 'confidence' => 1],
                4 => ['actual' => 1900, 'confidence' => 1],
                5 => ['actual' => 1219.51, 'confidence' => 2],
                6 => ['actual' => 1000, 'confidence' => 0],
            ],
            'App\Models\BidLineItem' => [
                1 => ['actual' => 400, 'confidence' => 2],
                2 => ['actual' => 140, 'confidence' => 2],
                3 => ['actual' => 210, 'confidence' => 2],
                4 => ['actual' => 350, 'confidence' => 2],
                5 => ['actual' => 487.80, 'confidence' => 2],
                6 => ['actual' => 792.68, 'confidence' => 2],
                7 => ['actual' => 725.45, 'confidence' => 2],
                8 => ['actual' => 1174.55, 'confidence' => 2],
                9 => ['actual' => 1219.51, 'confidence' => 3],
                10 => ['actual' => 1000, 'confidence' => 0],
            ]
        ], $modifiedData);
    }

    public function testLineItemHasOriginalValueZero()
    {
        $sourceData = self::simpleBidStructure();
        $sourceData->get('App\Models\BidComponent')->get(5)->cost = 0;
        $sourceData->get('App\Models\BidLineItem')->get(9)->cost = 0;

        $modifiedData = $this->setStructureActuals([
            'App\Models\BidComponent' => [
                1 => ['actual' => 5500, 'confidence' => null],
                2 => ['actual' => 700, 'confidence' => null],
                3 => ['actual' => 4400, 'confidence' => null],
                4 => ['actual' => 1900, 'confidence' => null],
            ]
        ], $sourceData);

        self::$svc->propagate($modifiedData->get('App\Models\Bid')->get(1));

        $this->assertStructure([
            'App\Models\Bid' => [
                1 => ['actual' => 6500, 'confidence' => 2]
            ],
            'App\Models\BidComponent' => [
                1 => ['actual' => 5500, 'confidence' => 1],
                2 => ['actual' => 700, 'confidence' => 1],
                3 => ['actual' => 4400, 'confidence' => 1],
                4 => ['actual' => 1900, 'confidence' => 1],
                5 => ['actual' => 0, 'confidence' => 2],
                6 => ['actual' => 1000, 'confidence' => 0],
            ],
            'App\Models\BidLineItem' => [
                1 => ['actual' => 400, 'confidence' => 2],
                2 => ['actual' => 140, 'confidence' => 2],
                3 => ['actual' => 210, 'confidence' => 2],
                4 => ['actual' => 350, 'confidence' => 2],
                5 => ['actual' => 487.80, 'confidence' => 2],
                6 => ['actual' => 792.68, 'confidence' => 2],
                7 => ['actual' => 725.45, 'confidence' => 2],
                8 => ['actual' => 1174.55, 'confidence' => 2],
                9 => ['actual' => 0, 'confidence' => 3],
                10 => ['actual' => 1000, 'confidence' => 0],
            ]
        ], $modifiedData);
    }

    public function testAnActualIsZero()
    {
        $modifiedData = $this->setStructureActuals([
            'App\Models\BidComponent' => [
                1 => ['actual' => 5500, 'confidence' => null],
                2 => ['actual' => 700, 'confidence' => null],
                3 => ['actual' => 4400, 'confidence' => null],
                4 => ['actual' => 0, 'confidence' => null],
            ]
        ]);

        self::$svc->propagate($modifiedData->get('App\Models\Bid')->get(1));

        $this->assertStructure([
            'App\Models\Bid' => [
                1 => ['actual' => 6500, 'confidence' => 2]
            ],
            'App\Models\BidComponent' => [
                1 => ['actual' => 5500, 'confidence' => 1],
                2 => ['actual' => 700, 'confidence' => 1],
                3 => ['actual' => 4400, 'confidence' => 1],
                4 => ['actual' => 0, 'confidence' => 1],
                5 => ['actual' => 2146.34, 'confidence' => 2],
                6 => ['actual' => 1000, 'confidence' => 0],
            ],
            'App\Models\BidLineItem' => [
                1 => ['actual' => 400, 'confidence' => 2],
                2 => ['actual' => 140, 'confidence' => 2],
                3 => ['actual' => 210, 'confidence' => 2],
                4 => ['actual' => 350, 'confidence' => 2],
                5 => ['actual' => 858.54, 'confidence' => 2],
                6 => ['actual' => 1395.12, 'confidence' => 2],
                7 => ['actual' => 0, 'confidence' => 2],
                8 => ['actual' => 0, 'confidence' => 2],
                9 => ['actual' => 2146.34, 'confidence' => 3],
                10 => ['actual' => 1000, 'confidence' => 0],
            ]
        ], $modifiedData);
    }

    public function testBidAndOneComponentHaveManualActuals()
    {
        $modifiedData = $this->setStructureActuals([
            'App\Models\Bid' => [
                1 => ['actual' => 4000, 'confidence' => null]
            ],
            'App\Models\BidComponent' => [
                1 => ['actual' => 2100, 'confidence' => null],
            ]
        ]);

        self::$svc->propagate($modifiedData->get('App\Models\Bid')->get(1));

        $this->assertStructure([
            'App\Models\Bid' => [
                1 => ['actual' => 4000, 'confidence' => 1]
            ],
            'App\Models\BidComponent' => [
                1 => ['actual' => 2100, 'confidence' => 1],
                2 => ['actual' => 196.26, 'confidence' => 2],
                3 => ['actual' => 1884.11, 'confidence' => 2],
                4 => ['actual' => 1079.44, 'confidence' => 3],
                5 => ['actual' => 392.52, 'confidence' => 3],
                6 => ['actual' => 1900, 'confidence' => 2],
            ],
            'App\Models\BidLineItem' => [
                1 => ['actual' => 19.63, 'confidence' => 2],
                2 => ['actual' => 39.25, 'confidence' => 3],
                3 => ['actual' => 58.88, 'confidence' => 3],
                4 => ['actual' => 98.13, 'confidence' => 3],
                5 => ['actual' => 157.01, 'confidence' => 3],
                6 => ['actual' => 255.14, 'confidence' => 3],
                7 => ['actual' => 412.15, 'confidence' => 4],
                8 => ['actual' => 667.29, 'confidence' => 4],
                9 => ['actual' => 392.52, 'confidence' => 4],
                10 => ['actual' => 1900, 'confidence' => 3],
            ]
        ], $modifiedData);
    }

    public function testBidAndMultipleComponentsHaveManualActuals()
    {
        $modifiedData = $this->setStructureActuals([
            'App\Models\Bid' => [
                1 => ['actual' => 4000, 'confidence' => null]
            ],
            'App\Models\BidComponent' => [
                2 => ['actual' => 1500, 'confidence' => null],
                3 => ['actual' => 600, 'confidence' => null],
            ]
        ]);

        self::$svc->propagate($modifiedData->get('App\Models\Bid')->get(1));

        $this->assertStructure([
            'App\Models\Bid' => [
                1 => ['actual' => 4000, 'confidence' => 1]
            ],
            'App\Models\BidComponent' => [
                1 => ['actual' => 3658.12, 'confidence' => 2],
                2 => ['actual' => 1500, 'confidence' => 1],
                3 => ['actual' => 600, 'confidence' => 1],
                4 => ['actual' => 343.75, 'confidence' => 2],
                5 => ['actual' => 125, 'confidence' => 2],
                6 => ['actual' => 341.88, 'confidence' => 2],
            ],
            'App\Models\BidLineItem' => [
                1 => ['actual' => 1558.12, 'confidence' => 3],
                2 => ['actual' => 300, 'confidence' => 2],
                3 => ['actual' => 450, 'confidence' => 2],
                4 => ['actual' => 750, 'confidence' => 2],
                5 => ['actual' => 50, 'confidence' => 2],
                6 => ['actual' => 81.25, 'confidence' => 2],
                7 => ['actual' => 131.25, 'confidence' => 3],
                8 => ['actual' => 212.50, 'confidence' => 3],
                9 => ['actual' => 125, 'confidence' => 3],
                10 => ['actual' => 341.88, 'confidence' => 3],
            ]
        ], $modifiedData);
    }

    public function testOnlyBidHasManualActual()
    {
        $modifiedData = $this->setStructureActuals([
            'App\Models\Bid' => [
                1 => ['actual' => 23400, 'confidence' => null]
            ],
        ]);

        self::$svc->propagate($modifiedData->get('App\Models\Bid')->get(1));

        $this->assertStructure([
            'App\Models\Bid' => [
                1 => ['actual' => 23400, 'confidence' => 1]
            ],
            'App\Models\BidComponent' => [
                1 => ['actual' => 21400, 'confidence' => 2],
                2 => ['actual' => 2000, 'confidence' => 3],
                3 => ['actual' => 19200, 'confidence' => 3],
                4 => ['actual' => 11000, 'confidence' => 4],
                5 => ['actual' => 4000, 'confidence' => 4],
                6 => ['actual' => 2000, 'confidence' => 2],
            ],
            'App\Models\BidLineItem' => [
                1 => ['actual' => 200, 'confidence' => 3],
                2 => ['actual' => 400, 'confidence' => 4],
                3 => ['actual' => 600, 'confidence' => 4],
                4 => ['actual' => 1000, 'confidence' => 4],
                5 => ['actual' => 1600, 'confidence' => 4],
                6 => ['actual' => 2600, 'confidence' => 4],
                7 => ['actual' => 4200, 'confidence' => 5],
                8 => ['actual' => 6800, 'confidence' => 5],
                9 => ['actual' => 4000, 'confidence' => 5],
                10 => ['actual' => 2000, 'confidence' => 3],
            ]
        ], $modifiedData);
    }

    public function testBidAndTopAndSubHaveManualActual()
    {
        $modifiedData = $this->setStructureActuals([
            'App\Models\Bid' => [
                1 => ['actual' => 278000, 'confidence' => null]
            ],
            'App\Models\BidComponent' => [
                2 => ['actual' => 8000, 'confidence' => null],
                6 => ['actual' => 136500, 'confidence' => null]
            ],
        ]);

        self::$svc->propagate($modifiedData->get('App\Models\Bid')->get(1));

        $this->assertStructure([
            'App\Models\Bid' => [
                1 => ['actual' => 278000, 'confidence' => 1]
            ],
            'App\Models\BidComponent' => [
                1 => ['actual' => 141500, 'confidence' => 2],
                2 => ['actual' => 8000, 'confidence' => 1],
                3 => ['actual' => 132123.71, 'confidence' => 3],
                4 => ['actual' => 75695.88, 'confidence' => 4],
                5 => ['actual' => 27525.77, 'confidence' => 4],
                6 => ['actual' => 136500, 'confidence' => 1],
            ],
            'App\Models\BidLineItem' => [
                1 => ['actual' => 1376.29, 'confidence' => 3],
                2 => ['actual' => 1600, 'confidence' => 2],
                3 => ['actual' => 2400, 'confidence' => 2],
                4 => ['actual' => 4000, 'confidence' => 2],
                5 => ['actual' => 11010.31, 'confidence' => 4],
                6 => ['actual' => 17891.75, 'confidence' => 4],
                7 => ['actual' => 28902.06, 'confidence' => 5],
                8 => ['actual' => 46793.81, 'confidence' => 5],
                9 => ['actual' => 27525.77, 'confidence' => 5],
                10 => ['actual' => 136500, 'confidence' => 2],
            ]
        ], $modifiedData);
    }
}
